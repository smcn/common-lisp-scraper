(load "~/quicklisp/setup.lisp")
(ql:quickload '(:dexador :plump :lquery :lparallel))
(declaim (optimize (speed 3) (debug 0) (safety 0)))

(defun link (url)
  (concatenate 'string "https://www.tesco.com" url))

(defun convert-to-products (items)
  "Map over the items and create a list containing the product name, url, image and the price if 
available."
  (mapcar
   #'(lambda (item)
       (list :PRODUCT (aref (lquery:$ item ".product-details--content h3 a" (text)) 0)
	     :URL     (link (aref (lquery:$ item ".product-details--content h3 a" (attr :href)) 0))
	     :IMAGE   (aref (lquery:$ item "img" (attr :src)) 0)
	     :PRICE   (let ((price-vector (lquery:$ item ".price-per-sellable-unit" (text))))
			(if (> (length price-vector) 0)
			    (remove #\Space (aref price-vector 0))
			    "Out of Stock"))))
   items))

(defun should-continue (curr-url next-url)
  "Determine whether we should continue recursing through the category pages. This is decided by asking
if the current-page doesn't have a page number (meaning it is the first page), and making sure that the
next page also has a page number (meaning we can visit it) and that the next page has a higher page
number than the current page."
  (labels ((get-page-num (url)
	     (let ((parts (uiop:split-string url :separator "=")))
	       (if (= 1 (length parts))
		   NIL
		   (values (parse-integer (cadr parts)))))))
    (let ((curr-page-num (get-page-num curr-url))
	  (next-page-num (get-page-num next-url)))
      (and
       (not (null next-page-num))
       (or  (null curr-page-num)
	    ;; (< curr-page-num next-page-num))))))
	    (< next-page-num 5)))))) ;; We just want to scrape the first four pages for demonstration.

(defun get-next-page (resp)
  "Create a string composed of the websites base url and the href of the next page button."
  (link (car (last (coerce (lquery:$ resp ".pagination-btn-holder a" (attr :href)) 'list)))))

(defun scrape-category (url)
  "Recursively walk through the category pages collecting the product tiles, use this to create a 
singular list."
  (let* ((response      (lquery:$ (initialize (dex:get url))))
	 (product-tiles (coerce (lquery:$ response ".product-list--list-item") 'list))
	 (next-page     (get-next-page response)))
    (append (convert-to-products product-tiles)
	    (if (should-continue url next-page)
		(scrape-category next-page)
		'()))))

(defun main ()
  (print (scrape-category "https://www.tesco.com/groceries/en-GB/shop/fresh-food/all")))

(sb-ext:save-lisp-and-die "scrape"
			  :executable t
			  :toplevel 'main)
